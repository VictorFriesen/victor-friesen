<!DOCTYPE html>
<html lang="en">
<head>
<!-- GOOGLE FONTS  -->
  <link href='http://fonts.googleapis.com/css?family=Kameron' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300' rel='stylesheet' type='text/css'>
<!-- END GOOGLE FONTS  -->
  <meta charset="utf-8">
	<title>Victor Friesen - Graphic Design</title>
	<meta name="description" content="Victor Friesen is a graphic designer in the Twin Falls, Magic Valley area of Idaho. I have 15 years of experience in print graphic design and have been designing web sites for about 7 years. I believe that a business needs to present themselves in a professional manner to the pubic. A good designer can help a business do this. Please look at my portfolio to get an idea of the work  have done in the past and please call or email me if you have any questons or if you are interested in a free quote." />
  	<meta name="keywords" content="Graphic Design, Web Design, Twin Falls, Magic Valley, Illustration, Brochures, Logos, Design, Design Services" />
	<meta name="robots" content="" />
	<link href="css/html5-doctor-reset-stylesheet.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/personal_style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/personal_style.css" media="screen" rel="stylesheet" type="text/css" />


</head>
<body id="graphic_design" >
<h1>Victor Friesen</h1>
<!--
<div class="wrapper">
<div class="centeredlogo thirty topLogo">
<a id="circle" />
<a id="square" />
</div>
</div>
-->
<!-- <a class="tagline" href="#">square peg round hole</a> -->
<img src="images/square_peg.png" class="logo_med_up50" />
<h2>graphic designer</h2>
<h3>208-650-8251 /<a href="mailto:vic@victorfriesen.com"> vic@victorfriesen.com</a> </h3>
<p><em>I am always available to answer questions or to give you a free estimate.</em></p>
<p>Unless it is the middle of the night - then I will be asleep.</p>

</body>
</html>