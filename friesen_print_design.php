<!DOCTYPE html>
<html lang="">
<head>
  <!-- GOOGLE FONTS  -->
  <link href='http://fonts.googleapis.com/css?family=Kameron' rel='stylesheet' type='text/css'>
  <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300' rel='stylesheet' type='text/css'>
<!-- END GOOGLE FONTS  -->
<meta charset="utf-8">
	<title>Victor Friesen - Print Design</title>
	<meta name="description" content="" />
  	<meta name="keywords" content="" />
	<meta name="robots" content="" />
		<link href="css/html5-doctor-reset-stylesheet.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/personal_style.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="css/personal_style.css" media="screen" rel="stylesheet" type="text/css" />
</head>
<body id="print_design">


<div class="header"><h1>Victor Friesen</h1>
<h2>graphic designer</h2>
</div>

<h3>portfolio</h3>
<ul class="portfolio_nav">
<li class="print_nav">print</li>
<li class="online_nav">online</li>
<li class="logo_nav">logo</li>
</ul>

<div class="infobar">
	<ul>
	<li >portfolio</li>
	<li>about me</li>
	<li>contact</li>
	</ul>
	
</div>
<div class="main_content">
<h4>print design</h4>

<div class="port_window_wrapper">
<div class="port_window">
<img src="images/orjam_brochure.jpg" alt="Oregon Jamboree Brochure" />
</div>
<p class="portfolio_description">Oregon Jamboree <span>Duotone Brochure</span></p>
</div>

<div class="port_window_wrapper">
<div class="port_window">
<img src="images/om_flyer2_800w.jpg" alt="Orange Mate Air Freshener Brochure"/>
</div>
<p class="portfolio_description" >Orange Mate Air Freshener <span>4-Color Brochure</span></p>
</div>

<div class="port_window_wrapper">
<div class="port_window">
<img src="images/pa_mag_ad_800w.jpg" alt="Petersen-Arne Magazine Advertisment" />
</div>
<p class="portfolio_description">Petersen-Arne <span>Magazine Advertisement</span></p>
</div>

<div class="port_window_wrapper">
<div class="port_window">
<img src="images/ae_flyer1_800w.jpg" alt="A &amp; E Security Flyer"/>
</div>
<p class="portfolio_description" >A &amp; E Security<span>4 Color Flyer</span></p>
</div>

<div class="port_window_wrapper">
<div class="port_window">
<img src="images/cw_flyer1_out_800w.jpg" alt="Cedarwood Products" />
</div>
<p class="portfolio_description">Cedarwood Products <span>4-Color Brochure</span></p>
</div>

<div class="port_window_wrapper">
<div class="port_window">
<img src="images/bbc_stat_800w.jpg" alt="Better Box Company Stationery"/>
</div>
<p class="portfolio_description" >Better Box Company<span>Stationery</span></p>
</div>

</div>
</body>
</html>